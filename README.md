Downloading repository
- Click on `Copy`-> `Clone with HTTPS`
-Run `GitBash Here` wherever you want project to be stored.

Running project
- Open the project in an arbitrary IDE
- Run terminal(on Windows or directly in IDE) and run following commands
    - `cd django_project`
    - `py manage.py runserver`
    - Use `CTRL + click` on IP address that you'll see
    - At your browser window you should see a login page.
