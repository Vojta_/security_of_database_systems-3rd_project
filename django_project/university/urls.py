
from django.urls import path
from . import views #'.' means current directory
#from django.contrib.auth import views as auth_views


urlpatterns = [
    path('', views.login_page, name = 'login'),
    path('logout', views.logout_page, name = 'logout'),
    path('structure/', views.structure, name='structure'),
    #path('structure/address', views.address, name='address'),
    path('address/', views.address, name='address'),
    path('contact/', views.contact, name='contact'),
    path('courses/', views.course, name='courses'),
    path('department/', views.department, name='department'),
    path('faculty/', views.faculty, name='faculty'),
    path('login_info/', views.login_info, name='login_info'),
    path('role/', views.role, name='role'),
    path('room/', views.room, name='room'),
    path('study_programme/', views.study_programme, name='study_programme'),
    path('user/', views.user, name='user'),
    #---------------------------------------CRUD--------------------------------------------
    path('add/', views.add_address, name='address_add'),#To insert new record to addresss table
    path('address/update/<int:id>/', views.update, name='address_update'),
    path('address/delete/<int:id>/', views.delete, name='address_delete')
]