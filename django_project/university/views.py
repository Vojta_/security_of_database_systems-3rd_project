from django.shortcuts import render, redirect
from django.http import HttpResponse
from .decorators import unauthenticated_user
from .models import address_table, contact_table, course_table, department_table, faculty_table, login_info_table, role_table, room_table, study_programme_table, user_table, structure
from django.contrib.auth.models import User, auth
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from .forms import AddressForm
from django.http import HttpResponseRedirect

# Create your views here.

#creating new fnc 'log_page' to handle the trafic between homepage of site

@unauthenticated_user
def login_page(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = auth.authenticate(request, username = username, password =password  )

        if user is not None:
            login(request , user)
            return redirect('structure')    
        else:
            messages.info(request, 'Username or password is not valid')

    context = {}
    return render(request,'login.html', context)

def logout_page(request):
    logout(request)
    return redirect("login")

@login_required(login_url = 'login')
def structure(request):
    strc = structure
    return render(request, 'structure.html',{"strc":strc})

@login_required(login_url = 'login')
def address(request):
    address_tab = address_table.objects.all()
    return render(request, 'address.html',{"address_tab":address_tab})

@login_required(login_url = 'login')
def contact(request):
    contact_tab = contact_table.objects.all()
    return render(request, 'contact.html',{"contact_tab":contact_tab})

@login_required(login_url = 'login')
def course(request):
    course_tab = course_table.objects.all()
    return render(request, 'course.html',{"course_tab":course_tab})

@login_required(login_url = 'login')
def department(request):
    department_tab = department_table.objects.all()
    return render(request, 'department.html',{"department_tab":department_tab})

@login_required(login_url = 'login')
def faculty(request):
    faculty_tab = faculty_table.objects.all()
    return render(request, 'faculty.html',{"faculty_tab":faculty_tab})

@login_required(login_url = 'login')
def login_info(request):
    login_info_tab = login_info_table.objects.all()
    return render(request, 'login_info.html',{"login_info_tab":login_info_tab})

@login_required(login_url = 'login')
def role(request):
    role_tab = role_table.objects.all()
    return render(request, 'role.html',{"role_tab":role_tab})

@login_required(login_url = 'login')
def room(request):
    room_tab = room_table.objects.all()
    return render(request, 'room.html',{"room_tab":room_tab})

@login_required(login_url = 'login')
def study_programme(request):
    study_programme_tab = study_programme_table.objects.all() 
    return render(request, 'study_programme.html',{"study_programme_tab":study_programme_tab})

@login_required(login_url = 'login')
def user(request):
    user_tab = user_table.objects.all()
    return render(request, 'user.html',{"user_tab":user_tab})



#-------------------CRUD for 'address_table'-----------------------------------------------------------------------------------------

 
@login_required(login_url = 'login')
def add_address(request):
    form = AddressForm(request.POST or None)
    address = address_table.objects.all()
    if form.is_valid():
        form.save()
    return render(request, 'address_add.html',{'form':form})

@login_required(login_url = 'login')
def update(request, id):
    address = address_table.objects.get(address_id = id)
    if request.method == 'POST':
        form = AddressForm(request.POST, instance = address)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/address')
    return render(request, 'address_update.html',{'address':address})

@login_required(login_url = 'login')
def delete(request, id):
    form = address_table.objects.get(id=id)
    form.delete()
    return HttpResponseRedirect('/address')


    