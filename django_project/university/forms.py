from django import forms
from django.db.models import fields
from .models import address_table


class AddressForm(forms.ModelForm):
    class Meta:
        model = address_table
        #fields = ('street','house_number','city','zip_code','country')
        fields = '__all__'
