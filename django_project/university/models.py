from django.db import models

class structure(models.Model):
    None

    
class user_table(models.Model):#each table is gonna have it's own class. 
    #Now creating attributes which are going to represent fields in the database
    user_id = models.IntegerField(primary_key=True)
    first_name = models.CharField(max_length=45)
    last_name = models.CharField(max_length=45)
    username = models.CharField(max_length=45)
    password = models.CharField(max_length=45)
    contact_id = models.IntegerField()
    class Meta:
        db_table = "user"


class address_table(models.Model):
    address_id = models.IntegerField(primary_key=True)
    street = models.CharField(max_length=45)
    house_number = models.CharField(max_length=45)
    city = models.CharField(max_length=45)
    zip_code = models.CharField(max_length=45)
    country = models.CharField(max_length=45)
    class Meta:
        db_table = "address"

class contact_table(models.Model):
    contact_id = models.IntegerField(primary_key=True)
    email = models.CharField(max_length=45)
    phone_number = models.CharField(max_length=45)
    class Meta:
        db_table = "contact"


class course_table(models.Model):
    course_id = models.IntegerField(primary_key=True)
    title = models.CharField(max_length=45)
    class Meta:
        db_table = "course"

class department_table(models.Model):
    department_id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=45)
    faculty_id = models.IntegerField()
    address_id = models.IntegerField()
    class Meta:
        db_table = "department"

class faculty_table(models.Model):
    faculty_id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=45)
    address_id = models.IntegerField()
    contact_id = models.IntegerField()
    class Meta:
        db_table = "faculty"

class login_info_table(models.Model):
    login_int_id = models.IntegerField(primary_key=True)
    sign_in_timestamp = models.TextField()
    sign_out_timestamp = models.TextField()
    user_id = models.IntegerField()
    class Meta:
        db_table = "login_info"

class role_table(models.Model):
    role_id = models.IntegerField(primary_key=True)
    role_type = models.CharField(max_length=45)
    class Meta:
        db_table = "role"

class room_table(models.Model):
    room_id = models.IntegerField(primary_key=True)
    building = models.CharField(max_length=45)
    floor = models.CharField(max_length=45)
    room_number = models.CharField(max_length=45)
    class Meta:
        db_table = "room"

class study_programme_table(models.Model):
    study_programme_id = models.IntegerField(primary_key=True)
    title = models.CharField(max_length=45)
    class Meta:
        db_table = "study_programme"





   